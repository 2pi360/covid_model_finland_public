import MCMC
import cProfile as prf
import pstats


#MCMC.MAIN(warmup=0, iterations_n=1000, trial_name="C:/history/profiling")

prf.run('MCMC.MAIN(warmup=0, iterations_n=1000, trial_name="C:/history/profiling")', 'profiling_output')

p = pstats.Stats('profiling_output')

p.sort_stats('cumulative').print_stats(50)
p.sort_stats('tottime').print_stats(50)

p.sort_stats('tottime').print_stats('useful.py:')
p.sort_stats('tottime').print_stats('model.py:')
p.sort_stats('tottime').print_stats('parameters.py:')
p.sort_stats('tottime').print_stats('numpy')

