from math import sqrt, exp
import numpy as np

# This is a generic 'adaptive proposal', which is instantiated
# for each 'sampling block':
class RobustAdaptiveMetropolis:
    """Simple implementation of the Robust Adaptive Metropolis sampler.

    Building blocks for the Robust Adaptive Metropolis sampler
    (doi:10.1007/s11222-011-9269-5).

    The module provides two methods, 'draw' and 'adapt', which draw
    random samples from the proposal, and adapt the proposal, respectively.
    They should be applied consecutively ('adapt' uses auxiliary variables
    stored in 'draw').
    """

    # Constructor: d is the dimension of the proposal:
    def __init__(self, d, alpha_opt=0.234, gamma=0.66):
        """Initialise the adaptive proposal distribution.

        :param d: state-space dimension
        :param alpha_opt: desired acceptance rate (default: 0.234)
        :param gamma: step size decay rate (default: 0.66)
        """

        self.d = d                 # Dimension
        self.z = np.zeros(d)       # Independent N(0,I) rv's
        self.chol = np.identity(d) # Proposal Cholesky factor
        self.alpha_opt = alpha_opt # Desired accept rate
        self.gamma = gamma         # Step size decay rate

    def draw(self):
        """Draw proposal increment."""

        # Draw a proposal (increment) z ~ N(0,I), output ~ N(0, chol*chol')
        # NB: It is important that 'z' is saved in the state (used in adapt)
        self.z = np.random.randn(self.d)
        return self.chol @ self.z

    # Adapt the proposal covariance:
    def adapt(self, alpha, j):
        """Adapt the proposal.

        :param alpha: acceptance probability of the last proposal.
        :param j: number of current iteration.
        """

        # Step size (as in the paper):
        step = min(1, self.d*pow(j+1, -self.gamma))

        # Difference of acceptance prob vs. desired:
        dalpha = alpha - self.alpha_opt

        # Calculate normalised 'innovation', avoiding division by zero:
        sz2 = sqrt(np.inner(self.z, self.z))
        normalised_z = self.chol @ (self.z/sz2 if sz2 > 0 else self.z)

        # Calculate new proposal covariance:
        cov = self.chol @ self.chol.transpose()
        cov += step * dalpha * np.outer(normalised_z, normalised_z)
        # ...and its Cholesky factor:
        self.chol = np.linalg.cholesky(cov)
        # (NB: This could also be done by a rank-1 Cholesky update/downdate,
        # which saves computations if d >> 1)
