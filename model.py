from math import log, exp, isnan

import numpy
import numba
from numpy import array, zeros, convolve

import useful
from read_data import MODEL_n, MODEL_T, POP_SIZES
from read_data import WARD, ICUC
from read_data import SEROLOGY_ALL, SEROLOGY_POS


def gamma_distr(T, mean, std):
    """
    discrete approximation of a gamma distribution
    """
    distr = [useful.dlgamma_unnormalized(t, mean=mean, var=std**2) for t in range(1, T)]
    distr = [x - max(distr) for x in distr]
    distr = [0] + [exp(x) for x in distr]
    return array(distr) / sum(distr)


# empirical distribution of duration spend in hospital
CONV_HOSP_y=[0.984382178, 0.820854401, 0.687643573, 0.57005057, 0.484611907, 0.421221934, 0.350482403, 0.299954172, 0.254938117, 0.226458576, 0.19981643, 0.178686456, 0.159393876, 0.147450853, 0.130914366, 0.118971352, 0.113459194, 0.103353575, 0.094166654, 0.083142358, 0.073955457, 0.069362011, 0.065687258, 0.061093821, 0.055581706, 0.050988285, 0.048232239, 0.045476197, 0.036289438, 0.028940121, 0.026184162, 0.025265516, 0.025265516, 0.024346873, 0.0225096, 0.019753731, 0.017916523, 0.017916523, 0.016079356, 0.012405221, 0.011486752, 0.010568322, 0.008731629, 0.007813405, 0.007813405, 0.005977388, 0.00505975, 0.004142573, 0.004142573, 0.004142573, 0.004142573, 0.002311701, 0.001402118, 0.00051581]
CONV_HOSP_o=[0.982156137, 0.817100409, 0.695167355, 0.582156238, 0.46468417, 0.388847785, 0.324907317, 0.272862766, 0.229740157, 0.194052502, 0.173234717, 0.146469017, 0.133086182, 0.12267732, 0.106320559, 0.100372656, 0.089963841, 0.08252899, 0.07212023, 0.06617239, 0.055763729, 0.051302906, 0.046842111, 0.040894442, 0.037920641, 0.033459999, 0.031973138, 0.03048629, 0.027512639, 0.026025841, 0.024539067, 0.024539067, 0.021565606, 0.021565606, 0.018592311, 0.014132904, 0.014132904, 0.012646678, 0.011160654, 0.009674924, 0.008189647, 0.008189647, 0.005221949, 0.00374169, 0.00374169, 0.00374169, 0.00374169, 0.00374169, 0.00226945, 0.000834884, 0.000834884, 0.000834884, 0.000834884, 0.000834884, 0.000834884, 0.000834884, 0.000834884]

# empirical distribution of duration spend in ICU
CONV_ICU_o=[0.982795726, 0.892473294, 0.827957282, 0.732839704, 0.680649499, 0.624110125, 0.571919952, 0.537126516, 0.489285562, 0.463190509, 0.432746293, 0.393603759, 0.367508757, 0.327897536, 0.275082695, 0.266137117, 0.225882114, 0.203518319, 0.194572827, 0.180838932, 0.171683033, 0.171683033, 0.157949239, 0.153371326, 0.148793422, 0.135059783, 0.121326279, 0.107592961, 0.103015243, 0.098437559, 0.098437559, 0.093859912, 0.08470476, 0.08470476, 0.069309124, 0.052685177, 0.052685177, 0.052685177, 0.052685177, 0.045671615, 0.038660188, 0.038660188, 0.031652284, 0.031652284, 0.031652284, 0.031652284, 0.031652284, 0.031652284, 0.031652284, 0.031652284, 0.022679853, 0.022679853, 0.022679853, 0.022679853, 0.022679853, 0.008343452, 0.008343452, 0.008343452]
CONV_ICU_y=[0.981651896, 0.871563502, 0.798171515, 0.724779829, 0.66973632, 0.614693095, 0.577997811, 0.559650239, 0.504607873, 0.467913319, 0.467913319, 0.429721849, 0.372435751, 0.372435751, 0.372435751, 0.312056873, 0.312056873, 0.25168219, 0.231558794, 0.191315657, 0.171196772, 0.151080621, 0.151080621, 0.151080621, 0.130968451, 0.130968451, 0.130968451, 0.070688988, 0.070688988, 0.070688988, 0.070688988, 0.070688988, 0.070688988, 0.070688988, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.050650873, 0.030721307, 0.030721307, 0.030721307, 0.030721307, 0.030721307, 0.030721307]


class Epidemic():
    """
    Model of the epidemic epidemic. When initializing the model, we can set T: number of days to model, as it
    may be different from the number of days we have in the data (i.e. if we want to make a prediction)
    """

    def __init__(self, T):
        self.T = T

        self.N = POP_SIZES[:]                # population size

        self.S = zeros((MODEL_n, T))         # susceptible
        self.E = zeros((MODEL_n, T))         # exposed
        self.I = zeros((MODEL_n, T))         # infectious
        self.R = zeros((MODEL_n, T))         # removed

        self.new_E = zeros((MODEL_n, T))     # new E per day
        self.new_hosp = zeros((MODEL_n, T))  # hospitalization rate
        self.new_icuc = zeros((MODEL_n, T))  # ICU rate

        self.SERO = zeros((MODEL_n, T))      # seropositives

        self.hos_distr = gamma_distr(T, mean=10, std=4)  # time from infection to Ward hospitalization
        self.icu_distr = gamma_distr(T, mean=11, std=4)  # time from infection to ICU hospitalization

    def sample(self, *, C_t, Ro_t, start, hosp_per_inf_a, icu_per_inf_a):
        """
        Simulate an epidemic with given set of parameters

        C_t: contact matrix for each day
        Ro_t: Basic reproduction number for each data.

        Contact matrixes would be calibrated to have a given Ro only using C_t[0] and Ro_t[0]

        start: time of the start of the outbreak, i.e. day when the number of new infections reach 1000

        hosp_per_inf_a: probability of hospitalization gived infection per age group
        icu_per_inf_a: probability of ICU admission gived infection per age group
        """
        # pre-set parameters
        E_dur = 3
        I_dur = 5

        # Next Generation Matrixes (NGM), used for simulation.
        # Here:
        # unit_Ro -- largest eigenvalue of the contact matrix (i.e. Ro assuming contact matrix is NGM)
        # stable_distr -- eigenvector corresponding to the largest eigenvalue of the contact matrix,
        #    i.e. relative distribution of infections among age group at the initial growth stage
        unit_Ro, stable_distr = useful.largest_r_eigenvector(C_t[0])
        self.unit_Ro = unit_Ro
        self.unit_NGM = numpy.copy(C_t[0] / unit_Ro)
        self.stable_distr = stable_distr

        # Initial condition
        # We want to set initial condition so that the number of new infection would be 1000 at the time "start"
        # Age groups are not used here.
        # Here r_per_day is the daily increase in the new cases and e_i_balance is a relative amount of E and I at the initial growth stage
        r_per_day, e_i_balance = useful.largest_r_eigenvector([[1-1/E_dur, Ro_t[0]/I_dur], [1/E_dur, 1-1/I_dur]])
        i_last = 1000 / (Ro_t[0] / I_dur)
        i_first = i_last * r_per_day**(-start + 1)
        e_first = i_first / e_i_balance[1] * e_i_balance[0]

        # split estimated initial condition by age groups
        I = i_first * stable_distr
        S = self.N - (i_first - e_first) * stable_distr
        E = e_first * stable_distr
        R = array([0.0]*MODEL_n)

        # Simulations
        for t in range(self.T):
            # move numbers between S, E, I and R compartments
            S_to_E = I @ C_t[t] / unit_Ro * Ro_t[t] / I_dur * S / self.N
            E_to_I = E / E_dur
            I_to_R = I / I_dur

            S = S - S_to_E
            E = E + S_to_E - E_to_I
            I = I          + E_to_I - I_to_R
            R = R                   + I_to_R

            # fix potential problems
            S[S<0] = 0

            # save values
            self.I[:, t] = I
            self.E[:, t] = E
            self.S[:, t] = S
            self.R[:, t] = R
            self.new_E[:, t] = S_to_E

        # Estimate Seropositives
        for a in range(MODEL_n):
            self.SERO[a, 14:] = self.R[a, :-14] / self.N[a]

        # Add a few summaries
        # attack rate till now
        self.attack_rate = (sum(self.N) - sum(S)) / sum(self.N)

        # current R
        C = array([[C_t[-1][i, s] * S[s] / self.N[s] for s in range(MODEL_n)] for i in range(MODEL_n)])
        self.current_Ro = useful.largest_eigenvalue(C) * Ro_t[t-1] / unit_Ro

        # R before reopening
        C = array([[C_t[99][i, s] * self.S[s, 99] / self.N[s] for s in range(MODEL_n)] for i in range(MODEL_n)])
        self.before_reopening = useful.largest_eigenvalue(C) * Ro_t[99] / unit_Ro

        # Estimate Hospitalization rate
        for a in range(MODEL_n):
            self.new_hosp[a, :]  = convolve(self.new_E[a, :], self.hos_distr)[:self.T] * hosp_per_inf_a[a]
            self.new_icuc[a, :]  = convolve(self.new_E[a, :], self.icu_distr)[:self.T] * icu_per_inf_a[a]


    def sample_extra(self, **pars):
        """
        Sample the epidemic, and then compute extra quantities which are not used to compute likelihood, but could be important to analyse.
        """
        self.sample(**pars)
        self._par_used = pars

        # compute ICU burden
        self.hosp_burden = [0]*self.T
        self.icu_burden = [0]*self.T
        for a in range(7):
            self.hosp_burden += convolve(self.new_hosp[a, :], CONV_HOSP_y)[:self.T]
            self.icu_burden  += convolve(self.new_icuc[a, :], CONV_ICU_y)[:self.T]
        for a in range(7, 9):
            self.hosp_burden += convolve(self.new_hosp[a, :], CONV_HOSP_o)[:self.T]
            self.icu_burden  += convolve(self.new_icuc[a, :], CONV_ICU_o)[:self.T]

        # compute mortality
        # probability of death given infection, %
        di_p = [0, 0, 0, 0, 0.01, 0.05, 0.5, 3.1,   5.6]
        ded_distr = gamma_distr(self.T, mean=18, std=6)

        self.mortality = zeros((MODEL_n, self.T))
        for a in range(9):
            self.mortality[a, :] = convolve(self.new_E[a, :], ded_distr)[:self.T] * di_p[a] / 100
        self.total_dead = self.mortality.sum()

        # compute Ro effective
        self.Ro_effective = [0] * self.T
        unit_Ro = self.unit_Ro
        for t in range(self.T):
            C = array([[pars['C_t'][t][i, s] * self.S[s, t] / self.N[s] for s in range(MODEL_n)] for i in range(MODEL_n)])
            self.Ro_effective[t] = useful.largest_eigenvalue(C) * pars['Ro_t'][t] / unit_Ro

    def __likelihood_old__(self, **pars):
        """
        old version of the likelihood function
        """
        self.sample(**pars)
        self.Likelihood = 0
        for a in range(MODEL_n):
            # ward hospitalizations
            for prediction, data in zip(self.new_hosp[a, :], WARD[a]):
                if not isnan(data):
                    self.Likelihood += useful.dlpois_un(data, prediction)

            # ICU hospitalizations
            for prediction, data in zip(self.new_icuc[a, :], ICUC[a]):
                if not isnan(data):
                    self.Likelihood += useful.dlpois_un(data, prediction)

            # Serological survey
            for pred, data_pos, data_all in zip(self.SERO[a, :], SEROLOGY_POS[a], SEROLOGY_ALL[a]):
                self.Likelihood += useful.dlbinom(data_pos, N=data_all, p=pred * (0.993 + 0.975 - 1) + 0.025)

        return self.Likelihood

    def likelihood(self, **pars):
        """
        Returns the likelihood for the given parameters
        """
        self.sample(**pars)
        self.Likelihood = 0

        try:
            # ward hospitalizations
            self.Likelihood += dl_poisson_up_to_c(WARD, self.new_hosp).sum()
            # ICU hospitalizations
            self.Likelihood += dl_poisson_up_to_c(ICUC, self.new_icuc).sum()
            # Serological survey
            self.Likelihood += dl_binom_up_to_c(SEROLOGY_POS, SEROLOGY_ALL, self.SERO * (0.993 + 0.975 - 1) + 0.025).sum()
        except ValueError:
            self.Likelihood = float('-inf')

        return self.Likelihood


# Probability distributions are optimized: constants (i.e. factors that do not depend on parameters) are not computed,
# border assumptions of paramters are not tested.
# Also, function and vectorized using numba, so they can be applied to arrays and matrixes

@numba.vectorize([numba.float64(numba.int32, numba.float64)])
def dl_poisson_up_to_c(x, p):
    """ log PMF of a Poisson distribution, up to a constant. Assumes p != 0"""
    if x==0:
        return -p
    else:
        return x*log(p)-p


@numba.vectorize([numba.float64(numba.int32, numba.int32, numba.float64)])
def dl_binom_up_to_c(x, n, p):
    """ log PMF of a Binomoal distribution, up to a constant. Assumes n >= x, p != 0 and p != 1"""
    if n==0:
        return 0
    elif x==0:
        return n*log(1-p)
    elif n==x:
        return x*log(p)
    else:
        return x*log(p) + (n-x)*log(1-p)


if __name__ == '__main__':
    from parameters import Parameters

    epidemic = Epidemic(T=MODEL_T)
    par = Parameters(T=MODEL_T)
    L = epidemic.likelihood(**par.for_simulations())
    print(epidemic)
    print(L)
    print(epidemic.new_E)
    epidemic.sample_extra(**par.for_simulations())
