import numpy

import ram
from useful import rexp, print_progress, exp

import data_pers
import model
from read_data import DATA_DATES, POP_SIZE
from parameters import Parameters


# directory to save MCMC output
TRIAL_N = 'C:/history/covid_modelling'


MCMC_UPDATE_BATCHES = [
    ('before_change', 'after_change', 'log_Ro_1', 'log_Ro_2', 'start'),
    ('log_Ro_3',),
    tuple(('log_p_a', i) for i in range(9)),
    tuple(('log_hosp_per_inf_a', i) for i in range(9)),
    tuple(('log_icu_per_hosp_a', i) for i in range(9))
    ]


def MAIN(trial_name=TRIAL_N,
         warmup=5_000,
         start_adaptation=50,
         iterations_n=200_000,
         thinning=10):

    # prepare files to record MCMC output
    # this is a main file, it would contain our samples
    H = data_pers.ParallelDataWriter(trial_name, key='n')

    # set up
    epidemic      = model.Epidemic(T=len(DATA_DATES))
    epidemic_test = model.Epidemic(T=len(DATA_DATES))
    par = Parameters(T=len(DATA_DATES))
    L = epidemic.likelihood(**par.for_simulations())
    print('started with likelohood', L)

    # initialize proposal variance
    jump = {batch: ram.RobustAdaptiveMetropolis(len(batch)) for batch in MCMC_UPDATE_BATCHES}

    # here we will record proposed changes; used for adaptation of proposal variance
    proposed_changes = {batch: [] for batch in MCMC_UPDATE_BATCHES}
    proposed_probs = {batch: [] for batch in MCMC_UPDATE_BATCHES}
    print('ready')

    for iteration in range(iterations_n + warmup):
        #print info
        print_progress(iteration, iterations_n + warmup)

        ####################
        ##   MCMC sample  ##
        ####################
        for batch in MCMC_UPDATE_BATCHES:
            new_par = par.propose_new(batch, jump[batch])
            new_L = epidemic_test.likelihood(**new_par.for_simulations())
            jump_prob = (new_L + new_par.prior) - (L + par.prior)
            if numpy.isnan(jump_prob):
                jump_prob = float('-inf')

            # adapt proposal probability
            jump[batch].adapt(min(1, exp(jump_prob)), iteration)

            if jump_prob > -rexp(1):
                # jump is succesfull! replase current values
                L = new_L
                par = new_par
                epidemic_test, epidemic = epidemic, epidemic_test

        ####################
        ## SAVE ITERATION ##
        ####################
        if iteration == warmup:
            # when warmup is over, start writing into a new file
            H.new_file()

        if iteration % thinning == 0:
            # save MCMC sample and some additional quantities
            H.writerow(
                Prior=par.prior,
                Likelihood=L,
                Posterior=par.prior + L,

                S_at_the_end=sum(epidemic.S[:, -1]) / POP_SIZE,
                E_at_the_end=sum(epidemic.E[:, -1]) / POP_SIZE,
                I_at_the_end=sum(epidemic.I[:, -1]) / POP_SIZE,
                R_at_the_end=sum(epidemic.R[:, -1]) / POP_SIZE,

                current_Ro=epidemic.current_Ro,
                attack_rate=epidemic.attack_rate,

                **par.values)

    print_progress(iterations_n + warmup, iterations_n + warmup)
    print('done!')


if __name__=='__main__':
    print(MCMC_UPDATE_BATCHES)
    MAIN()
