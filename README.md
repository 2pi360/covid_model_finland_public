This files contain one of the models, used by THL COVID modelling group


# LIST OF FILES:

COVID_MCMC_details.pdf
- model description

MCMC.py
- main file. Running it will start the inference, which creates the output file with posterior samples

model.py
- describes the epidemiological model: algorithm for sampling from model given parameters, and a likelihood function.

parameters.py
- describes the parameters of the model in the form which is useful for running MCMC. Also contains a function for transforming from MCMC-friendly to model-friendly form.

read_data.py
- contains pseudodata

read_contacts.py
- contains contact matrix

useful.py
- collection of different utility functions

data_pers.py
- utility functions for reading and writing MCMC samples


# DATA AND VISUALIZATION

Due to limitations, parts of the code responsible for loading the data and visualizing the results were not provided.
The actual data used for inference is replaced by pseudodata. Only a demo of the visualization code is provided.


# INSTRUCTIONS

Run MCMC.py

The MCMC samples would be saved into 'C:/history/covid_modelling' folder, or any other folder defined by TRIAL_N in MCMC.py line 11.
The inference can take up to several hours, but the first (warm-up) samples should be available immediately
and could be read/analysed.

To read, analyse and visualize the produces samples, see visualization.ipynb (jupyted notebook file).
Alternatevely, one can run preview_samples.py to see that samples exists.
Samples are loaded as a pandas.DataFrame object, to save them into a different format see pandas.DataFrame documantaion.

Early (warm-up) iterations take longer to compute, as proposal distributions are tuned during warm-up.

Sometimes a warning appears: "RuntimeWarning: invalid value encountered in log". It is produces by numpy and is handled by MCMC code.


# REQUIREMENTS

Code was tested on

```
Python 3.6.1

numpy==1.12.1
scipy==0.19.0
pandas==0.20.1
```
