import datetime

from numpy import exp, log
from numpy import array
import numba

from useful import ParametersClass
from read_data import DATA_DATES, MODEL_n, POP_SIZE
from read_contacts import C_PER_DAY, C_old


# prior probability of non-IC hospitalization given infection, %
prior_hosp_per_inf = [0.005, 0.01, 0.11, 0.18,   0.24, 0.5, 2,    6,     6.5 ]

# prior probability of ICU cases given hospitalization, %
prior_icu_per_hosp = [8, 9, 13, 18,              25, 32, 38, 45/2,       51/3]

prior_hosp_per_inf = [x/100 for x in prior_hosp_per_inf]
prior_icu_per_hosp = [x/100 for x in prior_icu_per_hosp]


@numba.vectorize([numba.float64(numba.float64)])
def logistic_appr(x):
    """ logistic function, precise enougth to do model changes in Ro """
    if x < -9:
        return 0
    elif x > 9:
        return 1
    else:
        return 1/(1+exp(-x))


class Parameters(ParametersClass):
    def init_pars(self, T):
        """
        Initialize the parameters and set up the prios.
        Most of the parameters are kept in log-trasformed forms, so it is easier to run MCMC.
        As priors are defined on linar scale, prior definition takes log-transformations into account
        Constants are dropped from the priors
        """
        # number of modeled days and age groups
        self.model_T = T
        self.model_n = MODEL_n

        # Ro
        self['log_Ro_1'] = log(3)
        self['log_Ro_2'] = log(1.4)
        self['log_Ro_3'] = log(1.4)
        # ~ Gamma(3, 1) + 1
        self.set_log_prior('log_Ro_1', lambda y: 3 * y - 1 * exp(y))
        # ~ Gamma(1.4, 1)
        self.set_log_prior('log_Ro_2', lambda y: 1.4 * y - 1 * exp(y))
        self.set_log_prior('log_Ro_3', lambda y: 1.4 * y - 1 * exp(y))

        self['before_change'] = 20
        self['after_change'] = 30
        # ~ N(x, sd=5), before should be smaller that after
        self.set_log_prior('before_change', 'after_change', lambda a, b: (-((a - 29)**2 + (b - 29)**2 ) / 50) if a < b  else float('-inf'))

        # relative susceptibilities by age
        self['log_p_a'] = [0] * MODEL_n
        self.set_log_prior('log_p_a', lambda Y: sum(-y**2 / 2 * 16 for y in Y))

        # parameter to adjust model time with the calender time
        self['start'] = 18
        self.set_log_prior('start', lambda y: -(y-18)**2 / 50)

        # hospitalization parameters
        self['log_hosp_per_inf_a'] = [log(x) for x in prior_hosp_per_inf]
        self['log_icu_per_hosp_a'] = [log(x) for x in prior_icu_per_hosp]
        # ~ Exponential(1/prior value) for all a
        self.set_log_prior('log_hosp_per_inf_a', lambda Y: sum(y - exp(y)/m for y, m in zip(Y, prior_hosp_per_inf)))
        self.set_log_prior('log_icu_per_hosp_a', lambda Y: sum(y - exp(y)/m for y, m in zip(Y, prior_icu_per_hosp)))

    def for_simulations(self):
        """
        Return a list of parameters on linar scale, more convenient for simulation
        """
        Ro_1 = 1 + exp(self['log_Ro_1'])
        Ro_2 = exp(self['log_Ro_2'])
        Ro_3 = exp(self['log_Ro_3'])
        break_mean = (self['after_change'] + self['before_change']) / 2
        break_var  = (self['after_change'] - self['before_change']) / 7

        hosp_per_inf_a = exp(self['log_hosp_per_inf_a'])
        icu_per_hosp_a = exp(self['log_icu_per_hosp_a'])

        R_factor_1 = logistic_appr([(t-break_mean)/break_var for t in range(100)])
        R_factor_2 = logistic_appr([(t-107)/1 for t in range(100, self.model_T)])

        # contact matrix in the form of M[source][sink]:
        p = exp(self['log_p_a'])
        C = [[C_PER_DAY[i, s] * p[s] for s in range(MODEL_n)] for i in range(MODEL_n)]
        return {
            'hosp_per_inf_a': hosp_per_inf_a,
            'icu_per_inf_a': hosp_per_inf_a * icu_per_hosp_a,
            'start': self['start'],

            'Ro_t': [Ro_1 * (1-x) + Ro_2 * x for x in R_factor_1] + [Ro_2 * (1-x) + Ro_3 * x for x in R_factor_2],

            'C_t': [array(C)] * self.model_T,
             }

    def convert_all(self):
        conv = super().convert_all()

        conv['Ro'] = conv['Ro_t'][0]
        conv['Ro_2'] = conv['Ro_t'][-1]
        return conv


if __name__ == '__main__':
    par = Parameters(40)
    nat = par.for_simulations()
    print( nat )
    print( par.prior )
    print( par.all_params_with_prior() )
    print( nat['Ro_t'] )
    print( nat['C_t'][0] )
    print( nat['hi_p_a'] )
    print( nat['ii_p_a'] )
