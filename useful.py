import copy
from datetime import datetime
from math import inf

import numpy
from numpy import pi
from numpy import log, exp, mean, std, cov
from numpy import matrix, identity, array
from numpy import random
from numpy.linalg import cholesky, slogdet

from scipy.special import gammaln


def logit(x):
    return log(x/(1-x))


def logistic(x):
    return 1/(1+exp(-x))


def logsum(L):
    L = list(L)
    h = max(L)
    return h + log(sum(exp(x-h) for x in L))


class ZeroLikelihood(BaseException):
    pass


# -----------------------------------------
# -- matrixes
# -----------------------------------------

def invert_with_cholesky(M):
    chol = cholesky(M)
    inv = chol.I
    return inv.T * inv


def largest_eigenvalue(matrix):
    w, v = numpy.linalg.eig(array(matrix))
    # eigenvalues and eigenvectors are recieved unordered
    # take the largest ones
    return max(w).real


def largest_l_eigenvector(matrix):
    """
    Input matrix in the form of M[source][sink]:
    for the formula  I @ NGM
    """
    # matrix is transposed for the purpose of counting left eigenvectors
    w, v = numpy.linalg.eig(array(matrix).T)
    # eigenvalues and eigenvectors are recieved unordered
    # take the largest ones
    w, v = max(zip(w, v.T))
    if all(v < 0):
        v *= -1
    return w.real, v.real


def largest_r_eigenvector(matrix):
    """
    Input matrix in the form of M[sink][source]:
    for the formula  NGM @ I
    """
    w, v = numpy.linalg.eig(array(matrix))
    # eigenvalues and eigenvectors are recieved unordered
    # take the largest ones
    w, v = max(zip(w, v.T))
    if all(v < 0):
        v *= -1
    return w.real, v.real


def compute_unit_NGM(NGM):
    unit_R0 = largest_eigenvalue(NGM)
    return array(NGM) / unit_R0


# -----------------------------------------
# -- density and logdensities
# -----------------------------------------

_DLNORM_CONSTANT = -0.5*log(2*pi)
LOG_FACTORIAL = tuple([gammaln(x+1) for x in range(100)])
def lf(x):
    if x < 100:
        return LOG_FACTORIAL[x]
    else:
        return gammaln(x+1)


def dlpois(x, p):
    if x==0:
        return -p
    elif p==0:
        return -inf
    else:
        return x*log(p)-lf(x)-p


def dlpois_un(x, p):
    if x==0:
        return -p
    elif p==0:
        return -inf
    else:
        return x*log(p)-p


def dlbinom(x, N, p):
    if x==N==0:
        return 0
    elif p==x==0:
        return 0
    elif p==1 and x==N:
        return 0
    elif p in (0, 1):
        return -inf
    else:
        return lf(N) - lf(x) - lf(N-x) + x*log(p) + (N-x)*log(1-p)


def dlpolya(x, mean, over):
    """
    overdispersed poisson aka negative binomial aka Polya

    nbinom.pmf(x) = choose(x+n-1, n-1) * p**n * (1-p)**x
    """
    if mean == x == 0:
        return 0
    elif x > 0 and mean == 0:
        return -inf
    n = 1 / over
    p = n / (mean+n)
    return gammaln(x+n) - gammaln(n) - lf(x) + n*log(p) + x*log(1-p)


def dlmultinom_unnormalized(X, P):
    s = sum(P)
    P = [p/s for p in P]
    return sum(log(p)*x for x, p in zip(X, P))


def dlimproper_1x(x):
    if x == 0:
        return 0
    return -log(x)


def dlnorm(x, m, std):
    return _DLNORM_CONSTANT -log(std) - (x-m)**2 / (2*std**2)


def dllognorm(x, m, std):
    if x == 0: return -inf
    return _DLNORM_CONSTANT -log(std)-log(x) - (log(x)-log(m))**2 / (2*std**2)


def dlbeta(x, a, b):
    return (a-1)*log(x) + (b-1)*log(1-x)


def dlgamma(x, a, b):
    if x==0 and a>0:
        return float('-inf')
    return (a-1)*log(x) - b*x


def dlexp(x, l):
    return - l*x


def dlU01(x):
    return -1


def dlmultivariate_norm_unnormalized(x, inverse):
    ''' no normalizing constant'''
    x_T = matrix([[h] for h in x])
    return -0.5 * (x * inverse * x_T)[0,0]


def dlmultivariate_norm(x, covariance):
    """ no dimention-dependent constant here, only factor dependent on covariance """
    (sign, logdet) = slogdet(covariance)
    return -0.5 * logdet + dlmultivariate_norm_unnormalized(x, covariance.I)


def dlgamma_unnormalized(x, mean, var):
    beta = mean / var
    alpha = mean * beta
    return alpha*log(beta) + (alpha-1)*log(x) - beta*x


def d_gamma_unnormalized(x, mean, var):
    beta = mean / var
    alpha = mean * beta
    return beta**alpha * x**(alpha-1) * exp(-beta*x)


# -----------------------------------------
# -- sampling
# -----------------------------------------

rexp = random.exponential
rpois = random.poisson
rnorm = random.normal
rbeta = random.beta
rbinom = random.binomial


def rpolya(m, over):
    n = 1 / over
    p = n / (m+n)
    return random.negative_binomial(n, p)


def rmulnorm_cov(x, cov):
    """ sample multivaruate normal using covariance """
    return random.multivariate_normal(x, cov).tolist()


def rmulnorm_cholesky(mean, cholesky):
    """ sample multivaruate normal using cholesky """
    X = random.normal(size=len(mean))
    return (cholesky @ X + mean).A1.tolist()


def rmultinom(n, P):
    s = sum(P)
    P = [p/s for p in P]
    return list(random.multinomial(n, P))


# -----------------------------------------
# -- MCMC
# -----------------------------------------

def adapt_proposal_distr(n_pars, proposed_changes, proposed_probs):
    cov = numpy.zeros((n_pars, n_pars))
    for i in range(n_pars):
        for j in range(i, n_pars):
            cov[i, j] = cov[j, i] = mean([(change[i]*change[j]) * min(1, exp(prob)) for change, prob in zip(proposed_changes, proposed_probs)]) / 0.24

    return cov * pi + numpy.identity(n_pars) * 0.001


class ParametersClass():
    def init_pars(self):
        pass

    def __init__(self, *args, **kwargs):
        self.values = {}
        self._log_priors = {}
        self._prior_functions = {}
        self._cached_priors = {}
        self.init_pars(*args, **kwargs)

    def set_log_prior(self, *args):
        """
        Definethe log prior function. Arguments: list of parameter names, followed by the prior function
        One parameter can only be used in one prior function, but one prior can use several parameters.
        """
        *params, prior_func = args
        key = ' '.join(params)
        for par in params:
            if par in self._log_priors:
                raise Exception(f'prior balue for the parameter {par} already defined')
            self._log_priors[par] = key
        self._cached_priors[key] = None
        self._prior_functions[key] = lambda self: prior_func(*[self.values[par] for par in params])

    def marginal_prior(self, par):
        key = self._log_priors[par]
        if self._cached_priors[key] is None:
            self._cached_priors[key] = self._prior_functions[key](self)
        return self._cached_priors[key]

    def all_params_with_prior(self):
        """ List all parameters, with log prior defined """
        return list(self._log_priors.keys())

    @property
    def prior(self):
        """ Compute and return log prior """
        # compute all priors what have not been computed yet
        for key, val in self._cached_priors.items():
            if val is None:
                self._cached_priors[key] = self._prior_functions[key](self)
        return sum(self._cached_priors.values())

    def copy(self):
        """ crete a deep copy of a ParametersClass object"""
        new = self.__class__.__new__(self.__class__)
        new.__dict__.update(copy.deepcopy(self.__dict__))
        return new

    def __getitem__(self, par):
        """
        Return a value of one parameter.
        If parameter is scalar, par is the name of the parameter.
        If parameter is vector, par is either the name of the parameter (in this case whole vector is returned), or a tuple (name, index)
        (in this case single value from the vector is returned).
        """
        if isinstance(par, tuple):
            return self.values[par[0]][par[1]]
        else:
            return self.values[par]

    def __setitem__(self, par, val):
        """
        Set a value of one parameter.
        If parameter is scalar, par is the name of the parameter.
        If parameter is vector, par is either the name of the parameter (in this case whole vector is set), or a tuple (name, index)
        (in this case single value from the vector is set).

        Prior is updated
        """
        if isinstance(par, tuple):
            par, index = par
            self.values[par][index] = val
        else:
            self.values[par] = val
        if par in self._log_priors:
            self._cached_priors[self._log_priors[par]] = None

    def update(self, new_vals):
        """
        Update parameters with the new values taken from a dict, then updates the prior.
        Retuns the uptaded Parameters.
        """
        self.values.update(new_vals)
        for key in self._cached_priors:
            self._cached_priors[key] = self._prior_functions[key](self)
        return self

    def propose_new(self, params, jump):
        """
        Returns a copy of parameters, but with several values changed according to Gaussian proposal distribution.
        params is a list of parameter names. jump is a coveriance matrix.
        """
        new = self.copy()

        old_vals = [self[par] for par in params]
        new_vals = old_vals + jump.draw()

        for val, par in zip(new_vals, params):
            new[par] = val
        return new

    def convert_all(self):
        """
        Try to convert all values to natural form.
        This is usefull for visualization and other checks.
        """
        conv = self.for_simulations()
        for key, val in self.values.items():
            if key not in conv:
                conv[key] = self[key]

                if key.startswith('log_') and not key[4:] in conv:
                    if isinstance(self[key], list):
                        conv[key[4:]] = [exp(x) for x in self[key]]
                    else:
                        conv[key[4:]] = exp(self[key])

                if key.startswith('logit_') and not key[6:] in conv:
                    if isinstance(self[key], list):
                        conv[key[6:]] = [logistic(x) for x in self[key]]
                    else:
                        conv[key[6:]] = logistic(self[key])
        return conv


# -----------------------------------------
# -- logging
# -----------------------------------------

MILESTONES = [0, 1] + sum([[1*10**i, 2*10**i, 5*10**i] for i in range(1, 7)], [])


START_TIME = None
LAST_TIME = None
LAST_ITER = None
def progress_str(i, n):
    global START_TIME, LAST_ITER, LAST_TIME
    time_now = datetime.now()

    if i == 0:
        START_TIME = time_now
        msg = f'{time_now:%Y-%m-%d %H:%M} -- starting'
    elif i == n:
        iteration = (time_now - START_TIME) / i
        total_time = time_now - START_TIME
        msg = f'fin -- iter {i} {iteration}/i --  total time {total_time}'
    else:
        iteration = (time_now - LAST_TIME) / (i - LAST_ITER)
        time_end = time_now + iteration*(n-i)
        day_diff = (time_end.date() - time_now.date()).days
        diff_str = f' (+{day_diff})' if day_diff else ''
        msg = f'{time_now:%Y-%m-%d %H:%M} -- iter {i} -- expected in {time_end:%H:%M}{diff_str} -- {iteration}/i'

    LAST_TIME = time_now
    LAST_ITER = i
    return msg


def print_progress(i, n):
    if i in MILESTONES or i == n:
        print(progress_str(i, n), end='\r')
